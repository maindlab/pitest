﻿using System;
using System.IO;
using System.Threading;

namespace Pitest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("start");
            var a = DateTime.Now;
            var ev = new CountdownEvent(1);
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1_000_000_000, Event = ev});
            ev.Wait();
            Console.Write("1:   ");
            Console.WriteLine(DateTime.Now-a);
            a = DateTime.Now;
            ev = new CountdownEvent(4);
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ev.Wait();
            Console.Write("4:   ");
            Console.WriteLine(DateTime.Now-a);
            a = DateTime.Now;
            ev = new CountdownEvent(8);
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
          
            ev.Wait();
            Console.Write("8:   ");
            Console.WriteLine(DateTime.Now-a);
            a = DateTime.Now;
            ev = new CountdownEvent(12);
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            ThreadPool.QueueUserWorkItem(Pi.CountPi, new ThreadData() {Count = 1000000000, Event = ev});
            
           /* var a1 = new Thread(Pi.CountPi);
            var a2 = new Thread(Pi.CountPi);
            var a3 = new Thread(Pi.CountPi);
            var a4 = new Thread(Pi.CountPi);
            var a5 = new Thread(Pi.CountPi);
            var a6 = new Thread(Pi.CountPi);
            var a7 = new Thread(Pi.CountPi);
            var a8 = new Thread(Pi.CountPi);
            a1.Start(new ThreadData()
            {
                Count = 1000000000, Event = ev
            });
            a2.Start(new ThreadData()
            {
                Count = 1000000000, Event = ev
            });
            a3.Start(new ThreadData()
            {
                Count = 1000000000, Event = ev
            });
            a4.Start(new ThreadData()
            {
                Count = 1000000000, Event = ev
            });
            a5.Start(new ThreadData()
            {
                Count = 1000000000, Event = ev
            });
            a6.Start(new ThreadData()
            {
                Count = 1000000000, Event = ev
            });
            a7.Start(new ThreadData()
            {
                Count = 1000000000, Event = ev
            });
            a8.Start(new ThreadData()
            {
                Count = 1000000000, Event = ev
            });
            a1.Join();
            a2.Join();
            a3.Join();
            a4.Join();
            a5.Join();
            a6.Join();
            a7.Join();
            a8.Join();
            */
            ev.Wait();
            Console.Write("12:   ");
            Console.WriteLine(DateTime.Now-a);
            Console.WriteLine("stop all");
        }
    }
}