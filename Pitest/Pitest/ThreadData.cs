using System.Threading;

namespace Pitest
{
    public class ThreadData
    {
        public int Count { get; set; }
        public CountdownEvent Event { get; set; }
    }
}