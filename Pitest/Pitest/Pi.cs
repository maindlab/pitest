using System;

namespace Pitest
{
    public class Pi
    {
        public static void CountPi(object o)
        {
            var c = o as ThreadData;
            
            int d = 1;
            double pi = 0;
            for (int i = 0; i < c.Count; i++)
            {
                if (i % 2 == 0)
                    pi += 1.0 / d;
                else
                    pi -= 1.0 / d;
                d += 2;
            }

            //Console.WriteLine("kek");
            c.Event.Signal();
        }
    }
}